CREATE OR REPLACE VIEW courses.title_all_course AS
    SELECT title
    FROM courses.course;
CREATE OR REPLACE VIEW courses.user_on_course AS
    SELECT username FROM courses.users
	LEFT OUTER JOIN courses.course_users ON courses.users.id = courses.course_users.users_id
	ORDER BY courses.users.username ASC
	;
	