SELECT comment.post_id, post.title, post.content FROM comment 
LEFT OUTER JOIN post ON comment.post_id = post.post_id

WHERE post.title ~ '^[0-9]' AND LENGTH(post.content) > 20
GROUP BY comment.post_id, post.title, post.content 
HAVING COUNT(comment.comment_id) = 2
ORDER BY post_id ASC
