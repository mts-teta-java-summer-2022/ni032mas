(SELECT post.post_id AS post_id FROM post
LEFT OUTER JOIN comment ON post.post_id = comment.post_id
WHERE comment.post_id is NULL)

UNION

(SELECT post.post_id AS post_id FROM post 
LEFT OUTER JOIN comment ON post.post_id = comment.post_id
WHERE comment.post_id NOTNULL)

ORDER BY post_id ASC
LIMIT 3
