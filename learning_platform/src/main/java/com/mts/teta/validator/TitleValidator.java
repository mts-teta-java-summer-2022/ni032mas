package com.mts.teta.validator;

import com.mts.teta.annotations.TitleCase;
import java.util.regex.Pattern;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TitleValidator implements ConstraintValidator<TitleCase, String> {

  private TitleValidatorType type;

  private static String patternPrepositionEn = "((?<!^\\b)\\bA\\b)|((?<!^\\b)\\bBut\\b)|((?<!^\\b)\\bFor\\b)|((?<!^\\b)\\bOr\\b)|((?<!^\\b)\\bNot\\b)|((?<!^\\b)\\bThe\\b)|((?<!^\\b)\\bAn\\b))";
  private static Pattern enPattern = Pattern.compile("(^([\\s.,:]|[a-z])|\\s$|\\s{2,}|[^\\sA-z\"',:]|" + patternPrepositionEn);

  private static Pattern ruPattern = Pattern.compile("^([\\s.,:]|[а-я])|\\s$|\\s{2,}|[^\\sА-Яа-я\"',:]|(\\s[А-Я])");

  public enum TitleValidatorType {
    RU,
    EN,
    ANY
  }

  @Override
  public void initialize(TitleCase constraintAnnotation) {
    this.type = constraintAnnotation.type();
  }

  @Override
  public boolean isValid(String title, ConstraintValidatorContext constraintValidatorContext) {
    switch (type) {
      case EN:
        return !enPattern.matcher(title).find();
      case RU: {
        return !ruPattern.matcher(title).find();
      }
      case ANY: {
        return matchAnyTitle(title);
      }
      default:
        return matchAnyTitle(title);
    }
  }

  private static boolean matchAnyTitle(String title) {
    if (!title.isBlank() && Pattern.compile("^[А-Я]").matcher(title).find() ) {
      return !ruPattern.matcher(title).find();
    } else {
      return !enPattern.matcher(title).find();
    }
  }

//  ((?:[a-zA-Z]+[А-Яа-я]|[А-Яа-я]+[a-zA-Z])[a-zA-ZА-Яа-я]*)
}
