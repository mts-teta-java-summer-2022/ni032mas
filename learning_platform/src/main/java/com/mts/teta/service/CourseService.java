package com.mts.teta.service;

import com.mts.teta.dao.entity.Course;
import com.mts.teta.dao.entity.Lesson;
import com.mts.teta.dao.entity.User;
import com.mts.teta.dto.UserDto;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CourseService {
  List<Course> findAllCourses();

  Optional<Course> findCoursesById(Long id);

  Course saveCourse(Course course);

  void deleteCourseById(Long id);

  List<Course> findCourseByTitleWithPrefix(String prefix);

  List<Lesson> findAllLesson();

  List<Lesson> findLessonByCourseId(Long courseId);

  Lesson saveLesson(Lesson lesson);

  Course assignUser(Long courseId, Long userId);

  User saveUser(User user);

  List<User> findAllUsers();

  Optional<Lesson> findLessonById(Long lessonId);

  void deleteLessonById(Long lessonId);
}
