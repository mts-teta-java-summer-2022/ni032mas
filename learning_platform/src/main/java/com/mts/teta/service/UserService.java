package com.mts.teta.service;

import com.mts.teta.dao.entity.*;
import java.util.*;

public interface UserService {

  User saveUser(User user);

  List<User> findAllUsers();

  Optional<Role> findRoleByName(String role);

}
