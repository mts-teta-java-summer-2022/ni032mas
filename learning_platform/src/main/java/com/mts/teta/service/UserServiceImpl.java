package com.mts.teta.service;

import com.mts.teta.dao.*;
import com.mts.teta.dao.entity.*;
import java.util.*;
import lombok.*;
import org.springframework.stereotype.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;
  private final RoleRepository roleRepository;

  @Override
  public User saveUser(User user) {
    return userRepository.save(user);
  }

  @Override
  public List<User> findAllUsers() {
    return userRepository.findAll();
  }


  @Override
  public Optional<Role> findRoleByName(String role) {
    return roleRepository.findByName(role);
  }
}
