package com.mts.teta.service;

import com.mts.teta.dao.*;
import com.mts.teta.dao.entity.*;
import java.util.*;
import lombok.*;
import org.springframework.stereotype.*;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

  private final CourseRepository courseRepository;
  private final LessonRepository lessonRepository;
  private final UserRepository userRepository;

  @Override
  public List<Course> findAllCourses() {
    return courseRepository.findAll();
  }

  @Override
  public Optional<Course> findCoursesById(Long id) {
    return courseRepository.findById(id);
  }

  @Override
  public Course saveCourse(Course course) {
    return courseRepository.save(course);
  }

  @Override
  public void deleteCourseById(Long id) {
    courseRepository.deleteById(id);
  }

  @Override
  public List<Course> findCourseByTitleWithPrefix(String prefix) {
    return courseRepository.findByTitleLike(prefix + "%");
  }

  @Override
  public List<Lesson> findAllLesson() {
    return lessonRepository.findAll();
  }

  @Override
  public List<Lesson> findLessonByCourseId(Long courseId) {
    return lessonRepository.findLessonByCourseId(courseId);
  }

  @Override
  public Lesson saveLesson(Lesson lesson) {
    return lessonRepository.save(lesson);
  }

  @Override
  public Course assignUser(Long courseId, Long userId) {
    User user = userRepository.findById(userId).orElseThrow(() -> new NoSuchElementException("Пользователь не найден!"));
    Course course = courseRepository.findById(courseId).orElseThrow(() -> new NoSuchElementException("Курс не найден!"));
    course.getUsers().add(user);
    user.getCourses().add(course);
    return courseRepository.save(course);
  }

  @Override
  public User saveUser(User user) {
    return userRepository.save(user);
  }

  @Override
  public List<User> findAllUsers() {
    return userRepository.findAll();
  }

  @Override
  public Optional<Lesson> findLessonById(Long lessonId) {
    return lessonRepository.findById(lessonId);
  }

  @Override
  public void deleteLessonById(Long lessonId) {
    lessonRepository.deleteById(lessonId);
  }
}
