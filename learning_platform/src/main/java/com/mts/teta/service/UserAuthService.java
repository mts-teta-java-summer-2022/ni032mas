package com.mts.teta.service;

import com.mts.teta.dao.*;
import java.util.stream.*;
import lombok.*;
import org.springframework.security.core.authority.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

@Service
@RequiredArgsConstructor
public class UserAuthService implements UserDetailsService {

  private final UserRepository userRepository;

  @Transactional(readOnly = true)
  @Override
  public UserDetails loadUserByUsername(String username) {
    return userRepository.findByUsername(username)
        .map(user -> new User(
            user.getUsername(),
            user.getPassword(),
            user.getRole().stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList())))
        .orElseThrow(() -> new UsernameNotFoundException("User not found"));
  }
}