package com.mts.teta.controller;

import com.mts.teta.dao.entity.*;
import com.mts.teta.dto.*;
import com.mts.teta.mapper.*;
import com.mts.teta.service.*;
import java.util.*;
import java.util.stream.*;
import javax.servlet.http.*;
import javax.validation.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.security.access.annotation.*;
import org.springframework.transaction.annotation.*;
import org.springframework.web.bind.annotation.*;


import static java.util.Objects.*;

@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController {

  private final CourseService courseService;
  private final CourseMapper courseMapper;
  private final LessonMapper lessonMapper;

  @GetMapping
  public List<CourseDto> courseTable(HttpSession session) {
    return courseService
        .findAllCourses()
        .stream()
        .map(courseMapper::toCourseDto)
        .collect(Collectors.toList());

  }

  @GetMapping("/{id}")
  public ResponseEntity<CourseDto> findById(@PathVariable("id") Long id) {
    return ResponseEntity.of(courseService.findCoursesById(id).map(courseMapper::toDto));
  }

  @Secured("ROLE_ADMIN")
  @PutMapping("/{id}")
  public void updateCourse(@PathVariable("id") Long id, @Valid @RequestBody CourseDto courseDto) {
    Course course = courseService.findCoursesById(id).orElseThrow(() -> new NoSuchElementException("Курс не найден!"));
    course.setAuthor(courseDto.getAuthor());
    course.setTitle(courseDto.getTitle());
    courseService.saveCourse(course);
  }

  @Secured("ROLE_ADMIN")
  @PostMapping
  public void createCourse(@Valid @RequestBody CourseDto courseDto) {
    courseService.saveCourse(courseMapper.toEntity(courseDto));
  }

  @Secured("ROLE_ADMIN")
  @DeleteMapping("/{id}")
  public void deleteCourse(@PathVariable("id") Long id) {
    courseService.deleteCourseById(id);
  }

  @GetMapping("/filter")
  public List<Course> getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
    return courseService.findCourseByTitleWithPrefix(requireNonNullElse(titlePrefix, ""));
  }

  @Transactional
  @GetMapping("/lessons")
  public List<LessonDto> getLessons() {
    return courseService
        .findAllLesson()
        .stream()
        .map(lessonMapper::toDto)
        .collect(Collectors.toList());
  }

  @Transactional
  @GetMapping("/{id}/lessons")
  public List<LessonDto> getLessonsByCourseId(@PathVariable("id") Long courseId) {
    return courseService
        .findLessonByCourseId(courseId)
        .stream()
        .map(lessonMapper::toDto)
        .collect(Collectors.toList());
  }

  @Secured("ROLE_ADMIN")
  @PutMapping("/lessons/{lessonId}")
  public void updateLesson(
      @PathVariable("lessonId") Long lessonId,
      @Valid @RequestBody LessonDto lessonDto
  ) {
    Lesson lesson = courseService
        .findLessonById(lessonId)
        .orElseThrow(() -> new NoSuchElementException("Урок не найден"));
    lesson.setTitle(lessonDto.getTitle());
    lesson.setText(lessonDto.getText());
    courseService.saveLesson(lesson);
  }

  @Secured("ROLE_ADMIN")
  @DeleteMapping("/lessons/{lessonId}")
  public void deleteLesson(@PathVariable("lessonId") Long lessonId) {
    courseService.deleteLessonById(lessonId);
  }


  @PostMapping("/{id}/lessons")
  public void createLesson(@PathVariable("id") Long courseId, @Valid @RequestBody LessonDto lessonDto) {
    courseService.saveLesson(lessonMapper.toEntity(lessonDto, courseId));
  }


  @PostMapping("/{courseId}/assign")
  public CourseDto assignUser(@PathVariable("courseId") Long courseId, @RequestParam("userId") Long userId) {
    return courseMapper.toDto(courseService.assignUser(courseId, userId));
  }
}
