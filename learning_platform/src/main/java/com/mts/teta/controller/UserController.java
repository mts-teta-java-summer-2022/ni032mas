package com.mts.teta.controller;

import com.mts.teta.dao.entity.*;
import com.mts.teta.dto.*;
import com.mts.teta.mapper.*;
import com.mts.teta.service.*;
import java.util.*;
import java.util.stream.*;
import lombok.*;
import org.springframework.security.access.annotation.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/user")
@RequiredArgsConstructor
public class UserController {

  private final UserService userService;
  private final UserMapper userMapper;
  private final RoleMapper roleMapper;

  @Secured("ROLE_ADMIN")
  @PostMapping("/users")
  public User createUser(@RequestBody UserDto user) {
    User userEntity = userMapper.toEntity(user);
    Set<Role> roles = user.getRoles().stream().map(roleMapper::toEntity).collect(Collectors.toSet());
    userEntity.setRole(roles);
    return userService.saveUser(userEntity);
  }

  @Secured("ROLE_ADMIN")
  @GetMapping("/users")
  public List<UserDto> getUsers() {
    return userService
        .findAllUsers()
        .stream()
        .map(userMapper::toDto)
        .collect(Collectors.toList());
  }
}
