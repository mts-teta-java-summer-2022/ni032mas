package com.mts.teta.dao.entity;

import javax.persistence.*;
import lombok.Data;

@Entity
@Table(name = "lessons", schema = "courses")
@Data
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    private String title;

    @Lob
    @Column
    private String text;

    @ManyToOne(optional = false)
    @JoinColumn(name="course_id", nullable=false)
    private Course course;
}
