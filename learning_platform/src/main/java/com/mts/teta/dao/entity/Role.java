package com.mts.teta.dao.entity;

import java.util.*;
import javax.persistence.*;
import lombok.*;

@Entity
@Table(name = "roles", schema = "courses")
@Data
public class Role {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column(unique = true)
  private String name;

  @ManyToMany
  @ToString.Exclude
  private Set<User> users;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Role role = (Role) o;
    return name.equals(role.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}