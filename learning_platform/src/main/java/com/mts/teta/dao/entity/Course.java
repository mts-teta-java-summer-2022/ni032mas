package com.mts.teta.dao.entity;

import com.mts.teta.annotations.*;
import com.mts.teta.validator.*;
import java.util.*;
import javax.persistence.*;
import lombok.*;

@Entity
@Table(name = "course", schema = "courses")
@Data
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "author")
    private String author;

    @TitleCase(type = TitleValidator.TitleValidatorType.EN)
    @Column(name = "title")
    private String title;

    @OneToMany(mappedBy = "course", orphanRemoval = true,  cascade = CascadeType.ALL)
    @ToString.Exclude
    private List<Lesson> lessons;

    @ManyToMany
    @ToString.Exclude
    private Set<User> users;
}
