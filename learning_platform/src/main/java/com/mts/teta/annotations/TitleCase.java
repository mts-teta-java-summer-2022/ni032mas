package com.mts.teta.annotations;

import com.mts.teta.validator.TitleValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = TitleValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TitleCase {

  TitleValidator.TitleValidatorType type() default TitleValidator.TitleValidatorType.ANY;

  //error message
  String message() default "Заголовок должен начинаться с букв";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
