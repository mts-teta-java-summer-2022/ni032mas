package com.mts.teta.handlers;

import com.mts.teta.dto.ApiError;
import java.time.OffsetDateTime;
import java.util.NoSuchElementException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(EmptyResultDataAccessException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<Object> handleEmptyResultDataAccessException(
      EmptyResultDataAccessException emptyResultDataAccessException
  ) {
    return new ResponseEntity<>(
        new ApiError(emptyResultDataAccessException.getMessage(), OffsetDateTime.now()),
        HttpStatus.NOT_FOUND
    );
  }

  @ExceptionHandler(NoSuchElementException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ResponseEntity<Object> handleNoSuchElementException(
      NoSuchElementException noSuchElementException
  ) {
    return new ResponseEntity<>(
        new ApiError(noSuchElementException.getMessage(), OffsetDateTime.now()),
        HttpStatus.NOT_FOUND
    );
  }
}