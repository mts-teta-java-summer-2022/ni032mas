package com.mts.teta.dto;

import javax.validation.constraints.NotNull;
import lombok.*;

@Data
public class LessonDto {
    @NotNull(message = "Поле заголовок не может быть пустым")
    private String title;
    @NotNull(message = "Поле текст не может быть пустым")
    private String text;
}