package com.mts.teta.dto;

import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseDto {

    @NotNull(message = "Поле автор не может быть пустым")
    private String author;

    @NotNull(message = "Название не может быть пустым")
    private String title;

    private List<UserDto> users;

}
