package com.mts.teta.dto;

import java.util.*;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
  @NotNull(message = "Поле userName не может быть пустым")
  private String username;

  private Set<RoleDto> roles;
}
