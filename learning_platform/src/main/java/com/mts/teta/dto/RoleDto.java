package com.mts.teta.dto;

import javax.validation.constraints.*;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleDto {
  @NotNull(message = "Поле name не может быть пустым")
  private String name;
}
