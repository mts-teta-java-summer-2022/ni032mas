package com.mts.teta.mapper;

import com.mts.teta.dao.entity.*;
import com.mts.teta.dto.*;
import java.util.*;
import java.util.stream.*;
import org.jetbrains.annotations.*;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.*;

@Mapper(componentModel = "spring")
public abstract class CourseMapper {
  @Autowired
  private UserMapper userMapper;

  public abstract CourseDto toDto(Course source);

  public abstract Course toEntity(CourseDto source);

  public CourseDto toCourseDto(@NotNull Course source) {
    List<UserDto> users = source.getUsers().stream().map(userMapper::toDto).collect(Collectors.toList());
    return new CourseDto(
        source.getAuthor(),
        source.getTitle(),
        users
    );
  }
}
