package com.mts.teta.mapper;


import com.mts.teta.dao.entity.*;
import com.mts.teta.dto.*;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface RoleMapper {

  RoleDto toDto(Role source);

  Role toEntity(RoleDto source);

}
