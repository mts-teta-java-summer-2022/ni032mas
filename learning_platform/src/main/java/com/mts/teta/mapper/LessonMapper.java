package com.mts.teta.mapper;


import com.mts.teta.dao.entity.Lesson;
import com.mts.teta.dto.LessonDto;
import com.mts.teta.service.CourseService;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class LessonMapper {

  @Autowired
  private CourseService courseService;

  public abstract LessonDto toDto(Lesson lesson);

  public abstract Lesson toEntity(LessonDto source);

  public Lesson toEntity(LessonDto source, Long courseId) {
    Lesson lesson = toEntity(source);
    lesson.setCourse(courseService.findCoursesById(courseId).orElseThrow(() -> new IllegalArgumentException("Курс не найден")));
    return lesson;
  }
}
