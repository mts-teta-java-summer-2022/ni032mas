package com.mts.teta.mapper;


import com.mts.teta.dao.entity.*;
import com.mts.teta.dto.*;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface UserMapper {
  UserDto toDto(User source);

  User toEntity(UserDto source);
}
