package com.mts.teta.validator;

import com.mts.teta.annotations.TitleCase;
import java.lang.annotation.Annotation;
import javax.validation.Payload;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TitleValidatorTest {

  private final TitleValidator validator = new TitleValidator();

  TitleCase getTitleCase(TitleValidator.TitleValidatorType type) {
    return new TitleCase() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return null;
      }

      @Override
      public TitleValidator.TitleValidatorType type() {
        return type;
      }

      @Override
      public String message() {
        return null;
      }

      @Override
      public Class<?>[] groups() {
        return new Class[0];
      }

      @Override
      public Class<? extends Payload>[] payload() {
        //noinspection unchecked
        return new Class[0];
      }
    };
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "Правильный заголовок",
      "Правильный заголовок с запятой,"
  })
  void isValidRu(String value) {
    validator.initialize(getTitleCase(TitleValidator.TitleValidatorType.RU));
    assertTrue(validator.isValid(value, null));
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "НЕ Правильный заголовок",
      "Правильный заголовок с пробелом "
  })
  void isNotValidRu(String value) {
    validator.initialize(getTitleCase(TitleValidator.TitleValidatorType.RU));
    assertFalse(validator.isValid(value, null));
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "This Is Val for",
      "Valid Title for a an the or"
  })
  void isValidEn(String value) {
    validator.initialize(getTitleCase(TitleValidator.TitleValidatorType.EN));
    assertTrue(validator.isValid(value, null));
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "Not valid заголовок",
      "Not valid For a an the or"
  })
  void isNotValidEn(String value) {
    validator.initialize(getTitleCase(TitleValidator.TitleValidatorType.EN));
    assertFalse(validator.isValid(value, null));
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "Правильный заголовок",
      "Правильный заголовок с запятой,",
      "This Is Val for",
      "Valid Title for a an the or"
  })
  void isValidAny(String value) {
    validator.initialize(getTitleCase(TitleValidator.TitleValidatorType.ANY));
    assertTrue(validator.isValid(value, null));
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "НЕ Правильный заголовок",
      "Правильный заголовок с пробелом ",
      "Not valid заголовок",
      "Not valid For a an the or"
  })
  void isNotValidAny(String value) {
    validator.initialize(getTitleCase(TitleValidator.TitleValidatorType.ANY));
    assertFalse(validator.isValid(value, null));
  }
}