package com.mts.teta.validator;

import com.mts.teta.models.Message;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MessageValidatorTest {

  private final MessageValidator messageValidator = new MessageValidatorImpl();

  @ParameterizedTest
  @ValueSource(strings = {
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876543210\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+74576543211\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+798765430934\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876453947\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876565048\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79366543938\",\n" +
          "    \"enrichment\": {\n" +
          "        \"firstName\": \"Elvis\",\n" +
          "        \"lastName\": \"Ivanov\"\n" +
          "    }\n" +
          "}"
  })
  void isMessageValid(String content) {
    assertTrue(messageValidator.isMessageValid(new Message(content, Message.EnrichmentType.MSISDN)));
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\":: \"book_card\",\n" +
          "    \"msisdn\": \"+79876543210\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          ",    \"msisdn\": \"+74576543211\"\n" +
          "}",
  })
  void isJsonNotValid(String content) {
    assertFalse(messageValidator.isMessageValid(new Message(content, Message.EnrichmentType.MSISDN)));
  }

  @ParameterizedTest
  @ValueSource(strings = {
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"workAddress\": \"Moscow\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"workAddress\": \"Samara\"\n" +
          "}",
  })
  void messageValidForAllType(String content) {
    assertTrue(messageValidator.isMessageValid(new Message(content, Message.EnrichmentType.WORK_ADDRESS)));
  }
}