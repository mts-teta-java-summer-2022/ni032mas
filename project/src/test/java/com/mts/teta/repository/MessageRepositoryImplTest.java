package com.mts.teta.repository;

import com.mts.teta.models.Message;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

class MessageRepositoryImplTest {

  private final MessageRepository messageRepository = new MessageRepositoryImpl();

  private final int numberOfThreads = 200;
  private final ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
  private CountDownLatch latch = new CountDownLatch(numberOfThreads);

  @BeforeEach
  void setUp() {
    latch = new CountDownLatch(numberOfThreads);
  }

  @Test
  void setEnrichedMessage() throws InterruptedException {
    for (int i = 0; i < numberOfThreads; i++) {
      executorService.submit(() -> {
        messageRepository.setEnrichedMessage(new Message("", Message.EnrichmentType.MSISDN));
        latch.countDown();
      });
    }
    latch.await();
    assertEquals(numberOfThreads, messageRepository.getEnrichedMessages().size());
  }

  @Test
  void setUnEnrichedMessage() throws InterruptedException {
    for (int i = 0; i < numberOfThreads; i++) {
      executorService.submit(() -> {
        messageRepository.setUnEnrichedMessage(new Message("", Message.EnrichmentType.MSISDN));
        latch.countDown();
      });
    }
    latch.await();
    assertEquals(numberOfThreads, messageRepository.getUnEnrichedMessages().size());
  }
}