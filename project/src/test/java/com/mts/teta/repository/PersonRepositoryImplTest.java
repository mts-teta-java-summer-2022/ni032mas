package com.mts.teta.repository;

import com.mts.teta.models.Person;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;
public class PersonRepositoryImplTest {
  private final PersonRepository repository = new PersonRepositoryImpl();
  public static final List<Person> personList = initPersons();

  private static  List<Person> initPersons() {
    List<Person> personList = new ArrayList<>();
    personList.add(new Person("Vasya", "Ivanov", "+79876543210"));
    personList.add(new Person("Vasya", "Petrov", "+74576543211"));
    personList.add(new Person("Danya", "Pupkin", "+798765430934"));
    personList.add(new Person("Vera", "Brezhneva", "+79876453947"));
    personList.add(new Person("Maksim", "Galkin", "+79876565048"));
    personList.add(new Person("Bruce", "Lee", "+79366543938"));
    return personList;
  }

  @Test
  void getPersons() {
    repository.setPersons(personList);
    assertEquals(personList, repository.getPersons());
  }

  @Test
  void setPersons() {
    repository.setPersons(personList);
    assertEquals(personList, repository.getPersons());
  }

  @Test
  void shouldNotNullGetPerson() {
    repository.setPersons(personList);
    assertNotNull(repository.getPerson("+79876543210"));
  }

  @Test
  void shouldNullGetPerson() {
    assertNull(repository.getPerson("+79876543210"));
  }
}