package com.mts.teta.controller;

import com.mts.teta.models.Enrichment;
import com.mts.teta.models.Message;
import com.mts.teta.repository.*;
import com.mts.teta.service.EnrichmentService;
import com.mts.teta.service.EnrichmentServiceImpl;
import com.mts.teta.validator.MessageValidator;
import com.mts.teta.validator.MessageValidatorImpl;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

class EnrichServiceTest {

  private EnrichmentService enrichmentService;
  private final int numberOfThreads = 200;
  private final ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
  private CountDownLatch latch = new CountDownLatch(numberOfThreads);
  private final PersonRepository personRepository = new PersonRepositoryImpl();
  private final MessageRepository messageRepository = new MessageRepositoryImpl();
  private final MessageValidator messageValidator = new MessageValidatorImpl();
  private final Logger LOG = LoggerFactory.getLogger(EnrichServiceTest.class);
  private final String[] contents = new String[]{
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876543210\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+74576543211\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+798765430934\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876453947\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876565048\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79366543938r\",\n" +
          "    \"enrichment\": {\n" +
          "        \"firstName\": \"Elvis\",\n" +
          "        \"lastName\": \"Ivanov\"\n" +
          "    }\n" +
          "}"
  };

  @BeforeEach
  void setUp() {
    BasicConfigurator.configure();
    personRepository.setPersons(PersonRepositoryImplTest.personList);
    enrichmentService = new EnrichmentServiceImpl(messageValidator, personRepository, messageRepository);
    latch = new CountDownLatch(numberOfThreads);
  }

  @AfterEach
  void afterAll() {
    executorService.shutdownNow();
  }

  /**
   * Тест на обогащение MSISDN.
   */
  @ParameterizedTest
  @ValueSource(strings = {
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876543210\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+74576543211\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+798765430934\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876453947\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79876565048\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+79366543938\",\n" +
          "    \"enrichment\": {\n" +
          "        \"firstName\": \"Elvis\",\n" +
          "        \"lastName\": \"Ivanov\"\n" +
          "    }\n" +
          "}"
  })
  void shouldAllMessagesHaveEnrichment(String content) {
    Message message = new Message(content, Message.EnrichmentType.MSISDN);
    String enrichMessage = enrichmentService.enrich(message);
    try {
      JSONObject jsonObject = new JSONObject(enrichMessage);
      LOG.info(enrichMessage);
      assertTrue(jsonObject.has(Enrichment.class.getSimpleName().toLowerCase()));
    } catch (JSONException e) {
      fail(e);
    }
  }

  /**
   * Тест на не найденный номер.
   */
  @ParameterizedTest
  @ValueSource(strings = {
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+792876543210\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+74576543r211\"\n" +
          "}"
  })
  void notFoundMsisdn(String content) {
    Message message = new Message(content, Message.EnrichmentType.MSISDN);
    String enrichMessage = enrichmentService.enrich(message);
    try {
      JSONObject jsonObject = new JSONObject(enrichMessage);
      LOG.info(enrichMessage);
      String enrichmentKey = "enrichment";
      assertFalse(jsonObject.has(enrichmentKey));
    } catch (JSONException e) {
      fail(e);
    }
  }

  /**
   * Тест на не найденный номер.
   */
  @ParameterizedTest
  @ValueSource(strings = {
      "{\n" +
          "ттт    \"action\": \"button_click\",,,,\n" +
          "    \"page\": \"book_card\",\n" +
          "    \"msisdn\": \"+792876543210\"\n" +
          "}",
      "{\n" +
          "    \"action\": \"button_click\",\n" +
          "    \"page\": \"book_card\",,,\n" +
          "    \"msisdn\": \"+74576543211\"\n" +
          "}"
  })
  void nonJsonFormat(String content) {
    Message message = new Message(content, Message.EnrichmentType.MSISDN);
    String enrichMessage = enrichmentService.enrich(message);
    assertEquals(content, enrichMessage);
  }

  /**
   * Тест End-to-End.
   */
  @Test
  void shouldAllMessagesEnrichment() throws InterruptedException {
    latch = new CountDownLatch(contents.length);
    for (String content : contents) {
      executorService.submit(() -> {
        try {
          enrichmentService.enrich(new Message(content, Message.EnrichmentType.MSISDN));
        } catch (Exception e) {
          fail(e);
        }
        latch.countDown();
      });
    }
    latch.await();
    assertEquals(5, messageRepository.getEnrichedMessages().size());
    assertEquals(1, messageRepository.getUnEnrichedMessages().size());
  }
}