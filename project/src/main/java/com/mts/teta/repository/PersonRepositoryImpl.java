package com.mts.teta.repository;

import com.mts.teta.models.Person;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class PersonRepositoryImpl implements PersonRepository {

  private final CopyOnWriteArrayList<Person> persons = new CopyOnWriteArrayList<>();

  @Override
  public List<Person> getPersons() {
    return persons;
  }

  @Override
  public void setPersons(List<Person> persons) {
    this.persons.clear();
    this.persons.addAll(persons);
  }

  @Override
  public Person getPerson(String msisdn) {
    return persons
        .stream()
        .filter(person -> person.getMsisdn().equals(msisdn))
        .findFirst()
        .orElse(null);
  }
}
