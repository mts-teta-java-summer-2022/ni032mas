package com.mts.teta.repository;

import com.mts.teta.models.Message;
import java.util.concurrent.CopyOnWriteArrayList;

public class MessageRepositoryImpl implements MessageRepository {

  private final CopyOnWriteArrayList<Message> enrichedMessages = new CopyOnWriteArrayList<>();

  private final CopyOnWriteArrayList<Message> unEnrichedMessages = new CopyOnWriteArrayList<>();


  @Override
  public CopyOnWriteArrayList<Message> getUnEnrichedMessages() {
    return unEnrichedMessages;
  }

  @Override
  public CopyOnWriteArrayList<Message> getEnrichedMessages() {
    return enrichedMessages;
  }

  @Override
  public void setEnrichedMessage(Message message) {
    enrichedMessages.add(message);
  }

  @Override
  public void setUnEnrichedMessage(Message message) {
    unEnrichedMessages.add(message);
  }
}
