package com.mts.teta.repository;

import com.mts.teta.models.Message;
import java.util.List;

public interface MessageRepository {
  List<Message> getUnEnrichedMessages();

  List<Message> getEnrichedMessages();

  void setEnrichedMessage(Message message);

  void setUnEnrichedMessage(Message message);
}
