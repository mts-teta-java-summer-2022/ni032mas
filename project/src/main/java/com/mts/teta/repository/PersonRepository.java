package com.mts.teta.repository;

import com.mts.teta.models.Person;
import java.util.List;

public interface PersonRepository {
  List<Person> getPersons();
  void setPersons(List<Person> persons);
  Person getPerson(String msisdn);
}
