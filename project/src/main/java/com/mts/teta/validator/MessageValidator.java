package com.mts.teta.validator;

import com.mts.teta.models.Message;

public interface MessageValidator {
  boolean isMessageValid(Message message);
}
