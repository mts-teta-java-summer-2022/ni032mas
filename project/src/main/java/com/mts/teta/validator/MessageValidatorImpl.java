package com.mts.teta.validator;

import com.mts.teta.models.Message;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageValidatorImpl implements MessageValidator {

  @Override
  public boolean isMessageValid(Message message) {
    try {
      JSONObject jsonObject = new JSONObject(message.getContent());
      return jsonObject.has(message.getEnrichmentType().value);
    } catch (JSONException e) {
      return false;
    }
  }
}
