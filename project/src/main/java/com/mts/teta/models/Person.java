package com.mts.teta.models;

public class Person {
  private final String firstName;
  private final String lastName;
  private final String msisdn;

  public Person(String firstName, String lastName, String msisdn) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.msisdn = msisdn;
  }
  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getMsisdn() {
    return msisdn;
  }

}
