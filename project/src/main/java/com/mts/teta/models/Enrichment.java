package com.mts.teta.models;

public class Enrichment {
  private final String firstName;
  private final String lastName;
  private static final String key = "enrichment";

  public Enrichment(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getKey() {
    return key;
  }
}
