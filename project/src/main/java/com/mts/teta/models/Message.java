package com.mts.teta.models;

public class Message {
  private String content;
  private final EnrichmentType enrichmentType;

  public enum EnrichmentType {

    WORK_ADDRESS("workAddress"),
    MSISDN("msisdn");

    public final String value;

    EnrichmentType(String msisdn) {
      value = msisdn;
    }
  }

  public Message(String content, EnrichmentType enrichmentType) {
    this.content = content;
    this.enrichmentType = enrichmentType;
  }
  public void setContent(String content) {
    this.content = content;
  }

  public String getContent() {
    return content;
  }

  public EnrichmentType getEnrichmentType() {
    return enrichmentType;
  }
}
