package com.mts.teta.service;

import com.mts.teta.models.Enrichment;
import com.mts.teta.models.Message;
import com.mts.teta.models.Person;
import com.mts.teta.repository.MessageRepository;
import com.mts.teta.repository.PersonRepository;
import com.mts.teta.validator.MessageValidator;
import org.json.JSONException;
import org.json.JSONObject;

public class EnrichmentServiceImpl implements EnrichmentService {
  private final MessageValidator validator;
  private final PersonRepository personRepository;
  private final MessageRepository messageRepository;

  public EnrichmentServiceImpl(MessageValidator validator, PersonRepository personRepository, MessageRepository messageRepository) {
    this.validator = validator;
    this.personRepository = personRepository;
    this.messageRepository = messageRepository;
  }

  @Override
  public String enrich(Message message) {
    if (!validator.isMessageValid(message)) {
      return message.getContent();
    }
    if (message.getEnrichmentType() == Message.EnrichmentType.MSISDN) {
      return enrichWithMsisdn(message);
    } else {
      messageRepository.setUnEnrichedMessage(message);
      return message.getContent();
    }
  }

  private String enrichWithMsisdn(Message message) {
    try {
      JSONObject jsonObject = new JSONObject(message.getContent());
      Person person = personRepository.getPerson(jsonObject.getString(message.getEnrichmentType().value));
      if (person == null) {
        messageRepository.setUnEnrichedMessage(message);

        return message.getContent();
      }
      Enrichment enrichment = new Enrichment(person.getFirstName(), person.getLastName());
      JSONObject enrichmentObject = new JSONObject(enrichment);

      jsonObject.put(enrichment.getKey(), enrichmentObject);
      message.setContent(jsonObject.toString());
      messageRepository.setEnrichedMessage(message);

      return message.getContent();
    } catch (JSONException e) {
      messageRepository.setUnEnrichedMessage(message);

      return message.getContent();
    }
  }
}
