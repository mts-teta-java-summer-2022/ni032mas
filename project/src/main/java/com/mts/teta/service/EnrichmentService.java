package com.mts.teta.service;

import com.mts.teta.models.Message;

public interface EnrichmentService {
  String enrich(Message message);
}
